#!/bin/bash

docker-compose -f docker-compose.prod.yml down -v

cd wishlist_container/wishlist_api
git pull origin master
cd ../../
cd wishlist_container/frontend
git pull origin master
npm install
npm run build
cd ../../
rm -rf wishlist_container/wishlist_api/staticfiles
cp -r wishlist_container/frontend/build wishlist_container/wishlist_api/staticfiles
mkdir wishlist_container/frontend/server/certs
cp nginx/certificates/wishlist_api.crt wishlist_container/frontend/server/certs/cert.pem
cp nginx/certificates/wishlist_api.key wishlist_container/frontend/server/certs/key.pem
cd wishlist_container/frontend/server/
cd ../../../
git pull origin master

docker-compose -f docker-compose.prod.yml up -d --build
docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput
docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear